const btnCalcular = document.getElementById('btnCalcular');
const btnLimpiar = document.getElementById('btnLimpiar');
const historialList = document.getElementById('historial');
const valorAutoInput = document.getElementById('valorAuto');
const porcentajeInput = document.getElementById('porcentaje');
const plazosInput = document.getElementById('plazos');
let cotizacionCount = 1;

btnCalcular.addEventListener("click", function () {
    
    let valorAuto = parseFloat(valorAutoInput.value);
    let pInicial = parseFloat(porcentajeInput.value);
    let plazos = parseInt(plazosInput.value);

    
    if (isNaN(valorAuto) || isNaN(pInicial) || isNaN(plazos)) {
        alert("Por favor, ingrese los datos del automóvil.");
        return; 
    }

    
    let pagoInicial = valorAuto * (pInicial / 100);
    let totalFinanciar = valorAuto - pagoInicial;
    let pagoMensual = totalFinanciar / plazos;

    
    document.getElementById('pagoInicial').value = pagoInicial.toFixed(2);
    document.getElementById('totalfin').value = totalFinanciar.toFixed(2);
    document.getElementById('pagoMensual').value = pagoMensual.toFixed(2);

    
    const historialItem = document.createElement('li');
    historialItem.textContent = `Auto ${cotizacionCount}: Valor Auto $${valorAuto.toFixed(2)}, Porcentaje ${pInicial.toFixed(2)}%, Plazos: ${plazos}`;
    historialList.appendChild(historialItem); 

    
    cotizacionCount++;

    
    valorAutoInput.value = '';
    porcentajeInput.value = '';
    plazosInput.value = '';
});

btnLimpiar.addEventListener("click", function () {
    
    valorAutoInput.value = '';
    porcentajeInput.value = '';
    plazosInput.value = '';
    document.getElementById('pagoInicial').value = '';
    document.getElementById('totalfin').value = '';
    document.getElementById('pagoMensual').value = '';
});
