document.addEventListener("DOMContentLoaded", function () {
    const pesoInput = document.getElementById("peso");
    const alturaInput = document.getElementById("altura");
    const calcularBtn = document.getElementById("calcular");
    const imcInput = document.getElementById("imc");
    const categoriaInput = document.getElementById("categoria");
    const imagenIMC = document.getElementById("imagen-imc");

    calcularBtn.addEventListener("click", function () {
        const peso = parseFloat(pesoInput.value);
        const altura = parseFloat(alturaInput.value);

        if (isNaN(peso) || isNaN(altura) || altura <= 0 || peso <= 0) {
            alert("Por favor, ingrese valores válidos.");
            return;
        }

        const imc = peso / (altura * altura);
        imcInput.value = imc.toFixed(2);

        if (imc < 18.5) {
            categoriaInput.value = "Peso insuficiente";
            imagenIMC.src = "/img/01.png";
        } else if (imc < 25) {
            categoriaInput.value = "Peso saludable";
            imagenIMC.src = "/img/02.png";
        } else if (imc < 30) {
            categoriaInput.value = "Sobrepeso";
            imagenIMC.src = "/img/03.png";
        } else if (imc < 35) {
            categoriaInput.value = "Obesidad grado 1";
            imagenIMC.src = "/img/04.png";
        } else if (imc < 40) {
            categoriaInput.value = "Obesidad grado 2";
            imagenIMC.src = "/img/05.png";
        } else {
            categoriaInput.value = "Obesidad grado 3";
            imagenIMC.src = "/img/06.png";
        }
    });
});
