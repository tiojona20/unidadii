document.addEventListener("DOMContentLoaded", function() {

    let alumno=[{
        "matricula":"2021030142",
        "nombre" :"Aguilar Romero Jonathan Jesus",
        "grupo" : "TI-73",
        "carrera" : "Tecnologias de la Informacion",
        "foto" : "/img/2021030142.jpg"
    },];

    console.log( "Matricula : " + alumno.matricula);
    console.log("Nombre : " + alumno.nombre);

    alumno.nombre = "Pastrano Navarro Brandon Rogelio";
    console.log("Nuevo Nombre :" + alumno.nombre);

    let cuentaBanco = {
        "numero":"1001",
        "Banco" :"Banorte",
        cliente: {
            "nombre" : "Aguilar Jonathan",
            "fechaNac" : "2003-02-23",
            "sexo" : "M",
            "saldo" : "10000"
        }
    }

    console.log("Nombre :" + cuentaBanco.cliente.nombre);
    console.log("Saldo :" + cuentaBanco.saldo);
    cuentaBanco.cliente.sexo="F",
    console.log(cuentaBanco.cliente.sexo);


    let productos = [{
        "descripcion" :"Jabon en polvo",
        "codigo" : "1001",
    }, {}, {}, {}]


    document.getElementById("generarNumeros").addEventListener("click", function() {
        const cantidadNumeros = parseInt(document.getElementById("cantidadNumeros").value, 10);
        if (cantidadNumeros > 0) {
            const elementos = generarNumerosAleatorios(cantidadNumeros);
            let resultados = "";

            resultados += `<p>${mostrarElementos(elementos)}</p>`;
            resultados += `<p>${mostrarNumerosPares(elementos)}</p>`;
            resultados += `<p>${mostrarValorMayorYPosicion(elementos)}</p>`;
            resultados += `<p>${calcularPromedio(elementos)}</p>`;
            resultados += `<p>${mostrarValorMenorYPosicion(elementos)}</p>`;
            const simetrico = esArregloSimetrico(elementos);
            resultados += `<p>${mostrarSimetria(simetrico)}</p>`;

            document.getElementById("resultContent").innerHTML = resultados;
        }
    });
    function generarNumerosAleatorios(cantidad) {
        const numerosAleatorios = [];
        for (let i = 0; i < cantidad; i++) {
            numerosAleatorios.push(Math.floor(Math.random() * 100) + 1);
        }
        return numerosAleatorios;
    }
    
    function esArregloSimetrico(elementos) {
        const len = elementos.length;
        for (let i = 0; i < Math.floor(len / 2); i++) {
            if (elementos[i] !== elementos[len - 1 - i]) {
                return false;
            }
        }
        return true;
    }
    
    function mostrarElementos(elementos) {
        const elementosString = elementos.join(", ");
        return `Elementos del Array: ${elementosString}\n`;
    }

    function mostrarNumerosPares(elementos) {
        const pares = elementos.filter(numero => numero % 2 === 0);
        const paresString = pares.join(", ");
        return `Números Pares: ${paresString}\n`;
    }

    function mostrarValorMayorYPosicion(elementos) {
        const maximo = Math.max(...elementos);
        const posicion = elementos.indexOf(maximo);
        return `Valor Mayor: ${maximo}, Posición: ${posicion}\n`;
    }

    function calcularPromedio(elementos) {
        const promedio = elementos.reduce((a, b) => a + b, 0) / elementos.length;
        return `Promedio: ${promedio.toFixed(2)}\n`;
    }

    function mostrarValorMenorYPosicion(elementos) {
        const minimo = Math.min(...elementos);
        const posicion = elementos.indexOf(minimo);
        return `Valor Menor: ${minimo}, Posición: ${posicion}\n`;
    }

    function mostrarSimetria(simetrico) {
        const mensaje = simetrico ? "El arreglo es simétrico" : "El arreglo no es simétrico";
        return mensaje + "\n";
    }
});