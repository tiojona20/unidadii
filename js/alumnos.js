let alumno = [{
    "matricula":"2021030652",
    "nombre":"Quezada Lara Jesus Alejandro",
    "grupo":"TI-73",
    "carrera":"Tecnologias de la Informacion",
    "foto":'<img src="/img/8.png">' 
},{
    "matricula":"2021030328",
    "nombre":"Garcia Gonzalez Jorge Enrique",
    "grupo":"TI-73",
    "carrera":"Tecnologias de la Informacion",
    "foto":'<img src="/img/2.jpeg">'
},{
    "matricula":"2021030652",
    "nombre":"Pastrano Navarro Brandon Rogelio",
    "grupo":"TI-73",
    "carrera":"Tecnologias de la Informacion",
    "foto":'<img src="/img/3.png">'
},{ 
    "matricula":"2021030142",
    "nombre":"Aguilar Romero Jonathan Jesus",
    "grupo":"TI-73",
    "carrera":"Tecnologias de la Informacion",
    "foto":'<img src="/img/4.jpeg">'
},{
    "matricula":"2021030143",
    "nombre":"Tirado Rios Luis Oscar",
    "grupo":"TI-73",
    "carrera":"Tecnologias de la Informacion",
    "foto":'<img src="/img/5.jpeg">'
},{
    "matricula":"2019030880",
    "nombre":"Quezada Ramos Julio Emiliano",
    "grupo":"TI-73",
    "carrera":"Tecnologias de la Informacion",
    "foto":'<img src="/img/6.png">'
},{
    "matricula":"2021030143",
    "nombre":"Tirado Rios Oscar",
    "grupo":"TI-73",
    "carrera":"Tecnologias de la Informacion",
    "foto":'<img src="/img/7.png">'
},{
    "matricula":"2021030311",
    "nombre":"Reyes Lizarraga Jonathan Alexis",
    "grupo":"TI-73",
    "carrera":"Tecnologias de la Informacion",
    "foto":'<img src="/img/1.jpeg">'
},{
    "matricula":"2021030314",
    "nombre":"Peñaloza Pizarro Felipe Andres",
    "grupo":"TI-73",
    "carrera":"Tecnologias de la Informacion",
    "foto":'<img src="/img/9.png">'
},{
    "matricula":"2020030321",
    "nombre":"Ontiveros Govea Yair Alejandro",
    "grupo":"TI-73",
    "carrera":"Tecnologias de la Informacion",
    "foto":'<img src="/img/10.png">'
}
];
let tabla = document.getElementById('tabla-alumnos');



for (i=0;i<alumno.length;i++){
    let row = tabla.insertRow();
 
let cell1 = row.insertCell(0);
let cell2 = row.insertCell(1); 
let cell3 = row.insertCell(2); 
let cell4 = row.insertCell(3);
let cell5 = row.insertCell(4); 


cell1.innerHTML = alumno[i].matricula;
cell2.innerHTML = alumno[i].nombre;
cell3.innerHTML = alumno[i].grupo;
cell4.innerHTML = alumno[i].carrera;
cell5.innerHTML = alumno[i].foto;
}
